import React from 'react'
import styled from 'styled-components'
import Collapsible from 'react-collapsible';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { RiErrorWarningFill } from 'react-icons/ri';

const StyledCard = styled.div`
    background: white;
    width: 530px;
    height: 470px;
    overflow-y: scroll;


    .statementText{
        font-family: 'Montserrat';
        background: #EEEEEE;
        padding-bottom: 20px;
        margin-left: 20px;
        margin-right: 20px;
        padding-left: 20px;
        font-size: smaller;
        padding-right: 20px;
        padding-top: 10px;
    }
    .statementTrigger{
        font-family:'Montserrat';
        font-size:smaller;
        font-weight:900;
        background:#EEEEEE;
        margin-top: 20px;
        margin-left: 20px;
        margin-right: 20px;
        height: 30px;
        padding-top: 6px;
        padding-left: 10px;

    }
    .react-tabs{
        margin: 25px;
    }
    .react-tabs__tab-panel{
        font-family:'Montserrat';
        border-left: 1px solid #aaa;
        border-right: 1px solid #aaa;
        border-bottom: 1px solid #aaa;
        padding:20px;
        padding: 20px;
        font-size: smaller;

    }
    .termIcon{
        margin-right: 5px
    }
`
const StyledTitle = styled.div`
    height:50px;
    padding-top:20px;
    text-align:center;
    font-family:'Montserrat';
    font-size:smaller;
    font-weight:900;
    background:#EEEEEE;

`

const collapseDiv = () => (

    <Collapsible
        trigger={<div className='statementTrigger'><RiErrorWarningFill className='termIcon'></RiErrorWarningFill>Statement of intent</div>}
    >
        <p className='statementText'>
            We want you to know exactly how our services work and why we need you details. Reviewing our policy will help you continue using the app with peace of mind.
            
        </p>
    </Collapsible>

)

const tabs = () => (
    <Tabs>
        <TabList>
            <Tab>Terms of Use</Tab>
            <Tab>Privacy Policy</Tab>
        </TabList>
        <TabPanel>
            <p>
                <p><b>We want you to understand the types of information we collect as you use our services</b>
                </p>
                <br></br>
                
                <p>
                    We collect information to provide better services to all our users — from figuring out basic stuff like which language you speak, to more complex things like which ads you’ll find most useful, the people who matter most to you online, or which YouTube videos you might like. The information Google collects, and how that information is used, depends on how you use our services and how you manage your privacy controls.

                    When you’re not signed in to a Google Account, we store the information we collect with unique identifiers tied to the browser, application, or device you’re using. This helps us do things like maintain your language preferences across browsing sessions.

                    When you’re signed in, we also collect information that we store with your Google Account, which we treat as personal information.
                </p>
                <br></br>
     
            </p>
            <p>
            <p><b>Things you create or provide to us</b></p>
                <br></br>
                <p>
                When you create a Google Account, you provide us with personal information that includes your name and a password. You can also choose to add a phone number or payment information to your account. Even if you aren’t signed in to a Google Account, you might choose to provide us with information — like an email address to receive updates about our services.

                We also collect the content you create, upload, or receive from others when using our services. This includes things like email you write and receive, photos and videos you save, docs and spreadsheets you create, and comments you make on YouTube videos.
                </p>
                <br></br>
     
            </p>

        </TabPanel>
        <TabPanel>
        <p>
        <p><b>Things you create or provide to us</b></p>
                <br></br>
                <p>
                When you create a Google Account, you provide us with personal information that includes your name and a password. You can also choose to add a phone number or payment information to your account. Even if you aren’t signed in to a Google Account, you might choose to provide us with information — like an email address to receive updates about our services.

                We also collect the content you create, upload, or receive from others when using our services. This includes things like email you write and receive, photos and videos you save, docs and spreadsheets you create, and comments you make on YouTube videos.
                </p>
                <br></br>
     
            </p>
        </TabPanel>

    </Tabs>
)


const Terms = () => (
    <StyledCard>
        <StyledTitle className='titleDiv'>Terms & Privacy Policy</StyledTitle>
        {collapseDiv()}
        {tabs()}

    </StyledCard>
)

export default Terms;