import React, { useState } from 'react';
import { Link } from 'react-router-dom'
import { authRequirements } from '../common/constants'
import Title from '../common/components/Title'
import Input from '../common/hookforms/Input'
import Submit from '../common/hookforms/Submit'
import Password from '../common/hookforms/Password'
import CenteredScreen from '../layout/CenteredScreen'
import { Form } from 'react-bootstrap'
import useForm from 'react-hook-form'
import styled from 'styled-components'
import AuthBanner from './AuthBanner'
import Popup from 'reactjs-popup';
import Terms from './Terms'
import { Button } from 'react-bootstrap';

const Wrapper = styled(CenteredScreen)`
  form{
    width:300px;
    text-align:left;
    margin:0 auto 15px;
    .submit{
      margin-top:42px;
      text-align:center;
    }
  }

`
const StyledTermButton = styled(Button)`
  margin-left: 10px;
  margin-right: 10px;
  border-radius: 20px!important;
`
const Login = ({ location, history }) => {
  const form = useForm({})
  const [openPopup, setOpenPopup] = useState(false);
  const [accept,setAccept] = useState(false)
  const onSubmit = (val) => { }
  return (<Wrapper className="screen-login">
    <Title title="Log in" />
    <Popup open={openPopup} onClose={() => setOpenPopup(false)} modal
      closeOnDocumentClick={false} closeOnEscape={false}>
      <div
        style={{
          backgroundColor: 'white',
          boxShadow: '0px 0px 7px 2px #9ca1a6',
          borderRadius: '15px',
        }}>
        <Terms />
        
        <div style={{ textAlign: 'center', padding: '20px' }}>
        <div style={{  paddingBottom: '10px' }}>
          <input type='checkbox' onClick={()=>{setAccept(!accept)}}></input>
          &nbsp; I have read and agree to Terms and Private Policy
          </div>
          <StyledTermButton onClick={()=>setOpenPopup(false)} >Cancel</StyledTermButton>
          <a href="register">
            <StyledTermButton
              style={{ backgroundColor: '#184771', color: 'white' }}
              disabled={!accept}
            >Accept</StyledTermButton></a>
        </div>
      </div>
    </Popup>
    <div className="container-fluid">
      <AuthBanner />
      <Form horizontal onSubmit={form.handleSubmit(onSubmit)}>
        <Input name="username" form={form} label="Username" minLength={authRequirements.USERNAME_MIN} required />
        <Password name="password" form={form} label="Password" required />
        <Submit className="submit" label="Sign in" form={form} primary />
      </Form>
      <p>
        <Link to="forgot" className="btn-forgot">Forgot your username or password?</Link>
      </p>
      <p>
        Don't have an account yet? &nbsp;
        <Link className="btn-registration" onClick={() => setOpenPopup(true)}>Create an account?</Link>
      </p>
    </div>
  </Wrapper>)
}
export default Login
